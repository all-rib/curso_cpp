# Planejamento.md

link de inscrição : https://forms.gle/fWULgNk5gutHaz8a6

As aulas são destinadas a todo, e qualquer, estudante da UTFPR.

Todas as aulas serão realizadas pelo Google Meet, com sala ainda a ser
gerada, que será encaminhada ao email dos inscritos. O curso terá a duração
estimada 6 a 12 horas, divididas em 3 a 6 aulas, de 2 horas cada

    Aula 1 - 10/07, das 14h às 16h
    Aula 2 - 11/07, das 14h às 16h
    Aula 3 - 18/07, das 14h às 16h
    Aula 4 - 24/07, das 14h às 16h(se necessária)
    Aula 5 - 25/07, das 14h às 16h(se necessária)
    Aula 6 - 01/08, das 14h às 16h(se necessária)

Este é um curso introdutório, sobre a sintaxe da linguagem C++, estruturas
de dados, e o paradigma de orientação a objetos. A ideia é habilitar os
participantes a ler, escrever e interpretar códigos fontes escrito em C++,
aplicando estruturação, básica, de dados, e o paradigma da orientação a
objetos.

As aulas serão feitas de forma síncrona, via plataforma online (Google
Meet). Elas também serão gravadas e disponibilizadas no youtube, e os links
dispostos na plataforma que hospeda o material de ensino (Gitlab). O
professor disponibilizará até três aulas, se necessário, para a implementação
de código, e atendimento às duvidas dos alunos.

Sem perfil específico para os alunos, qualquer um pode participar, desde
que apto a escrever um programa.

O material de estudo, e referências bibliográficas estão dispostas no
seguinte link:

'https://gitlab.com/kresserbash/curso_cpp'

As inscrições já estáo abertas, e o formulário ficará disponivel até
quarta-feira, 30 de junho, através do seguinte link:

'https://forms.gle/fWULgNk5gutHaz8a6'

Como pré-requisito, o aluno precisa ter um entendimento básico sobre
programação. Por basico entende-se conhecer as estruturas comuns de um
programa: variáveis, funções, e entrada e saída de dados.
