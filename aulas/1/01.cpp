#include <iostream>

int main(void)
{
	int a = 8;	
	std::cout << a << "\n"; //Impressão de um número
	a = a << 1;
	std::cout << a << "\n"; //Impressão de outro número
	std::cout << (a << 1) << "\n"; //Impressão do deslocamento de bit
}
