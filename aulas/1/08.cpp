#include <iostream>

int main(void)
{
	int lim = 20, val;
	std::cin >> lim;
	for (int i = 1; i <= lim; i++)
	{
		std::cout.width(i);
		std::cin >> val;
		std::cout << val << '\n';
	} 
}
