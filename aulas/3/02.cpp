#include <iostream>

//public: qualquer coisa acessa
//private: só a mesma classe acessa


class Teste
{
	public:
		int numero_pb;
		void setar_pb(int numero);
		void setar_pv(int numero);
	private:
		int numero_pv;

};

void Teste::setar_pb (int numero)
{	
	numero_pb = numero;
}

void Teste::setar_pv (int numero)
{	
	numero_pv = numero;
}




int main (void)
{
	Teste batata;
	batata.numero_pb = 10;
	//batata.numero_pv = 20;
	std::cout << batata.numero_pb;
	//std::cout << batata.numero_pv;
	batata.setar_pb(30);
	batata.setar_pv(40);
	std::cout << batata.numero_pb;
	//std::cout << batata.numero_pv;
}
