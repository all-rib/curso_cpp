#include <iostream>
#include <string.h>

class Carta
{
	public:
		char nome[30];
		char desc[200];
		int qtd;
		void init(char * n_nome, char * n_desc, int n_qtd);
		char rtn_nome(char * cpy);
		char rtn_desc(char * cpy);
		int rtn_qtd(void);

};

void Carta::init(char * n_nome, char * n_desc, int n_qtd)
{	
	strcpy(nome, n_nome);
	strcpy(desc, n_desc);
	qtd = n_qtd;
}

char Carta::rtn_nome(char * cpy)
{
	strcpy(cpy, nome);
}

char Carta::rtn_desc(char * cpy)
{
	strcpy(cpy, desc);
}

int Carta::rtn_qtd(void)
{
	return qtd;
}




int main (void)
{
	int max = 90;
	Carta baralho[max];
}
