#include <stdio.h>

#define CARTA struct carta

#define NOK 1
#define OK 0

CARTA
{
	int ata, def, qtd;
	char nome[20];
};

int init_carta(CARTA * nova_carta)
{
	nova_carta = (CARTA *)malloc(sizeof(CARTA));
	if (nova_carta == NULL)
	{
		return NOK;
	}
	return OK;
}

int main(void)
{
	int max = 30;
	CARTA * baralho[max];

	for (int i = 0; i < max; i++)
	{
		if (init_carta(baralho[i]) != OK)
		{
			return NOK;
		}
	}
	baralho[0]->nome = "carta teste";


}
