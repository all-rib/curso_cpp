#Debugging

Este é um resumo, de um resumo, de um resumo, sobre o uso do GDB.

Para um detalhamento claro, e REALMENTE aprender como o GDB funciona, use o
manual, que pode ser encontrado na página do
[software](https://gnu.org/software/gdb)

GDB é um software, livre, usado para debugar programas de diversas línguas:
C, C++, D, Fortran, etc.

Você pode encontrar o software [aqui](https://gnu.org/software/gdb), e
chamá-lo através de um terminal, ou ide, ou seu método preferido.

Para GNU/Linux, quando configurado corretamente:

```

[allan@home aulas]$ gdb programa

```

Os programas, quando compilados para release vem com várias flags de
otimização (-O), e o gdb lida bem com elas, mas para facilitar nosso
entendimento, é recomendável usar a flag de debug durante a compilagem:

```

[allan@home aulas]$ gcc -g programa1.c -o programa
[allan@home aulas]$ g++ -g programa1.cpp -o programa
[allan@home aulas]$ gdb programa1

```

## Listar o programa

Quando debugando, você pode sentir a necessidade de ver o source do seu
programa. Use "list"

```

list [linha]
list [função]
list

```

Respectivamente, o gdb irá imprimir algumas linhas ao redor de 'linha', irá
imprimir o source de 'função', e caso tenha ocorrido um 'list' antes, list
irá imprmir as linhas subsequentes das ultimas mostradas.

## Ver variáveis

Praticamente toda a informação que precisar sobre uma variável você
conseguirá usando 'print'.

## Breakpoints

Para criar um breakpoint em um arquivo/função/linha específica, use:

```

break [local]

```

E use o clear, com a mesma sintaxe, para remover o breakpoint. Para
remover toda uma lista de breakpoints, use:

```

delete [brekapoints][lista]

```

Para não ficar redigitando os breakpoints toda vez que recarregar seu
programa, use o save para salvar, e o source para recuperar.


```

save breakpoints [arquivo]
source [arquivo]

```

## Continue e step

Quando seu programa parar, você usa "continue" ou "c" para seguir com a
execução do programa. Caso, ainda, queira executar linha a linha, use o
"step"

## Stack

Informações sobre como seu programa chegou onde está, variáveis, chamadas,
e etc, use "backtrace", ou ainda "bt"
