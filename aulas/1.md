# 1 - Introdução à linguagem

## 1.0 - Exemplos de programa

[Programa 0000](aulas/1/00.cpp)

[Programa 0001](aulas/1/01.cpp)

[Programa 0011](aulas/1/02.cpp)

[Programa 0010](aulas/1/03.cpp)

[Programa 0110](aulas/1/04.cpp)

[Programa 0111](aulas/1/05.cpp)

[Programa 0101](aulas/1/06.cpp)

[Programa 0100](aulas/1/07.cpp)

[Programa 1100](aulas/1/08.cpp)

[Programa 1101](aulas/1/09.cpp)

[Programa 1111](aulas/1/10.cpp)

[Programa 1110](aulas/1/11.cpp)

Compile e rode.

```
[allan@home ~]$ g++ 1.cpp -o 1
[allan@home ~]$ ./1
```

## 1.1 - Diferenças de C para C++

### 1.1.1 - Linguagem Estruturada e linguagem Orientada a Objetos

A programação estruturada (PE), ou ainda linguagem estruturada (LE), denota
um paradigma de programação. O conceito vem da Engenharia de Software, e
denota uma abordagem da forma do programa em sí: como ele é escrito, como
as funções são separadas, acessos, chamadas, etc.

### 1.1.2 - Caracterísiticas da PE:

 * Sequenciamento: As tarefas ocorrem uma logo após a outra, até o fim;

 * Decisão: As tarefas podem ocorrer, ou não, após testes lógicos;

 * Iteração: Após testes lógicos, o código pode ser executado finitas vezes;

Na PE, a programação é tratada como um bloco único, segmentado em funções
atômicas, que organizam chamadas subsequentes para efetivar operações. É
praticamente melhor, por termos uma visão mais clara de como se organiza o
código. É um contraste à linguagens anteriores que possuiam o GOTO
(veja...).

Sua dominância se deu até a construção da Programação Orientada a Objetos
(POO). Na PE as funções tem controle de alto nível, refinamento de passos,
funções atômicas, e visão top-down (funções chamam outras funções).

### 1.1.3 - Características da POO:

A POO se baseia no conceito de classes, que definem objetos. Os objetos
possuem atributos, que são as variáveis, e aplicam os atributos sobre os
métodos, que são as funções, para realizar as operações.

Falaremos de POO, mais tarde...;

### 1.1.4 - Diferenças de código fonte

No source, não há, em geral, muitas diferenças do C. As mesmas diretivas de
pré-processamento são aceitas, como "#include" e "#define", e alguns
programadores gostam de usar ".cpp" como sufixo para diferenciar arquivos
de C (".c") e de C++. As mudanças vão começar nas palavras reservadas,
constantes de sistema, funções e estruturas exclusivas.

Por fim, se espera que eles sejam compilados de forma semelhante, certo?
Errado. Processador faz umas coisinhas a mais, que (talvez) veremos mais
tarde. O nome do compilador vai mudar também, mas foi implementado no mesmo
pacote do gcc. É o g++

### 1.1.5 - Prototipagem

Outra coisa diferente do C, é que o compilador é um pouco mais chato: Ele
EXIGE que haja protótipo para todas as funções. Não podem ser
"pré-definidas" como no C, por exemplo:

[Programa 0000](aulas/1/00.c)

[Programa 0000](aulas/1/00.cpp)

Apesar de não inserida, a biblioteca stdio.h será inclusa pelo gcc durante
a linkagem. Já no C++ isso não ocorre: Todas as bibliotecas precisam ser
devidamente incluídas, especificando o escopo de cada função previamente.
do contrário, erro de compilação.

## 1.2 - COUT, CIN, CERR e CLOG

### 1.2.1 - COUT

cout é um método da classe std. Ele faz o redirecionamento de toda a sua
entrada para a saída padrão (stdout), independente do que receba.

#### Para considerar

Se você for um mal observador, também vai ter percebido: mudamos a forma de
saída padrão do C para o C++, deixando o "printf" de lado, e adotando o
cout. Caso você nunca tenha se perguntado, a diretiva do sistema que indica
por onde o texto "sai", formalmente chamada de "saída padrão", é o
"stdout". Tudo que você encaminhar pra lá vai ser mostrado em algum lugar.
No GNU/Linux, o terminal enchaminha tudo o que você pedir pra lá, e os
programas, se você não especificar nada diferente, também.

Outra coisa importante de se notar é que "<<", em C, significa
"deslocamento, bit a bit, para a esquerda" (veja ..). Em C, o que seria
feito é o seguite:

[Programa 0001](aulas/1/01.c)

[Programa 0001](aulas/1/01.cpp)

No C++, ele também é um *OPERADOR DE CLASSES*. Talvez eu fale sobre isso,
mais tarde.

O que importa é que você está passando, a grosso modo, parâmetros para os
métodos dessas classes, e elas fazem o resto. E cout aceita, literalmente,
de tudo. Macros, variáveis, valores diretos, e por memória... Faça-se livre
para testar.

 * Lembre-se que o "\n" é mandatório. A saída padrão CONCATENTA TUDO;

Por fim, se você sabe como o printf do C funciona, deve estar se
perguntando:

 - Tá. Bacana todo esse texto. Mas e as variáveis? como eu imprimo as
   variáveis?

[Programa 0011](aulas/1/02.cpp)

Ué? E o tipo da variável? tipo... $!@#-se?

Então... Não tem tipo nessa ocasião... Sabe o python? A maior parte das
funções não exigem algum tipo específico para serem tratadas lá, certo. Não
é muito diferente aqui, com cout. Coloque lá, e ele faz o resto, desde que
seja uma variável, e não um objeto, ou endereço de memória. Mas não se
engane: todas as variáveis em C++ tem um tipo definido. Logo mais, vou
ensinar como vocês podem ver isso mais de perto.

O printf ainda funciona, no entanto. Veja esta quimera:

[Programa 0010](aulas/1/03.cpp)

### 1.2.2 - CIN

Bom. Existem entradas de dados, também. Para isto, usamos o método cin, ora
pois: da mesma forma que existe uma saída padrão, existe uma entrada
padrão. Mas tem algumas coisas diferentes, também...

#### Para considerar

Primeiro detalhe: Não existe '&'. Existe alguns conceitos em POO, e na
computação como um todo que devem ser aplicados, e fazem parte das Boas
Práticas de Programação, a começar pelo encapsulamento. Mas o importante:
as variáveis PRECISAM ser reais, e não ponteiros. Elas precisam ter nome
definido.

Outra coisa interessante: assim como no printf, cin há de usar
delimitadores para selecionar diferentes campos de informações. A não ser
que especificado outro, de maneira explícita, será usado o " ", ou SPACE
(0x20) em ascii. Então cuidado ao passar tipos como chars para ler um nome
completo. Não se preocupe com formatações por enquanto. Apenas fique atento
que armazenar "Bixuquinha" precisa de menos memória que "cabo o café".

### 1.2.3 - CERR

Além disso, também existe um lugar comum para onde vão todos os teus erros,
e ele não se chama ficha criminal, mas sim cerr. Basicamente, se seu
programa travar, fizer algo que não deve, uma função não compilar, perder a
referência de algo, vai parar em cerr. Mas a maioria dos ambientes de
programação encaminha esses erros diretos para o compilador, ou pra própria
saída padrão cout. Na dúvida, debugue.

#### 1.2.4 - CLOG

Clog faz praticamente a mesma coisa que cerr, mas opera em cima de um
buffer: ele precisa de uma instrução específica antes de liberar um bloco
de dados. Geralmente, uma nova entrada basta: um caractere branco (espaço,
enter, tab...). Você pode forçar isso, apesar de não ser recomendado: se
clog estiver rodando junto de vários processos paralelos, o buffer destes
podem dessincronizar.

[Programa 0110](aulas/1/04.cpp)

```
[allan@home 1]$ g++ 04.cpp -o 04
[allan@home 1]$ time ./04
```

## 1.3 - Uniões

Resumindo, uma união é uma estrutura de dados, onde você mapeia diversas
variáveis em um único endereço de memória. Gambiarra? não. Simplicidade: Se
você sabe como a memória funciona, vai conseguir lidar com os problemas que
podem acontecer no uso dessa estrutura.

[programa 0111](aulas/1/05.c)

No C++, as uniões também existem, mas com uma --gambiarra-- magia a mais:
uniões anonimas. Você não precisa nomear suas uniões, deste que não tenha
mais do que uma, e nem tenha variáveis com o mesmo nome das variáveis
dentro das uniões anônimas que criaste:

[programa 0111](aulas/1/05.cpp)

### Para considerar

Quer dizer: legal. Você pode guardar vários valores num dado endereço de
memória. O tipo de uma variável apenas determina o tamanho utilizado: int
usa 4 bytes (x64), e o char 2. Mas vou propor algumas perguntas aqui:

Mas por que usar union? Malloc faz a mesma coisa, e você não precisa
"tipar" uma variável. Só usar o endereço. 

Você pode declarar uma variável comum, já com tipo especificado, e
também usar para armazenar outros tipos de valores: uma letra cabe muito bem
num int.

Porque diabos vou me preocupar com várias variáveis no mesmo espaço. Isso
não é gambiarra? Me parece ridículo usar o mesmo espaço num papel para
fazer contas. Tem que tomar cuidado com memória, manter ela limpa, e
blablabla. Use as estruturas comuns...

## 1.4 - Variação de escopo

### 1.4.1 - Escopo global e escopo local

Excelete. Você está começando a entender um pouco das sintaxes. Mas você já
se perguntou o que acontece quando uma variável global e uma local tem o
mesmo nome? Como se referir a elas?

[programa 0101](aulas/1/06.cpp)

Como pode observar, o escopo global é acessável com "::variável". Na
verdade, você está fazendo um acesso de objeto. Para quem já estudou java,
sabe que precisa declarar uma classe global no início de cada programa. No
C++ não é diferente, mas isso é implícito.

### 1.4.2 - Padronização de parâmetros

Definido um escopo local, você pode precisar definir valores padrões para
variáveis: Quando definida uma função, e omite um parâmetro, por exemplo:

[programa 0100](aulas/1/07.cpp)

Mas saiba que não existe "salto" de parâmetros. Para n parâmetros, por
exemplo, se omitido um paramentro n-m, todos os m parâmetros sequentes
deverão ser omitidos. Pode fazer o teste alterando o source anterior.

## 1.5 - Fomatação de COUT

Quando lidando com saída, você pode querer formatar a impressão na tela de
forma específica, como por exemplo a largura de saída.

### 1.5.1 - Largura de saída, por cout

[programa 1100](aulas/1/08.cpp)

### 1.5.2 - Largura de saída, por setw

[programa 1101](aulas/1/09.cpp)

A lagura de saída só é aplicada para a próxima saída. Se fizer outra
impressão, precisará chamar 'cout.width()', ou 'setw()', novamente.

### 1.5.3 - Precisão, formato e bases

Por fim, você pode configurar qual caractere vai preencher os espaços em
branco, usando 'std::cout.fill()', de iostream, precisão de decimais com
'setprecision', de iomanip, configurar padrão de amostragem, usando as
flags 'ios::fixed' para formato fixo (com todas as casas à mostra) e
'ios::scientific' para formato cientíifico, repassando-as para o método
'setiosflags', além de resetar essas configurações com o método
'resetiosflags'

Outra coisa interessante é a conversão de bases: repasse seu número para
'setbase(base)', e essa função para o 'cout'.

### Considerações finais

Esse lance todo de tipos, entrada e saída de dados sem definir tipos te
parece legal? Se quiser ir um pouco mais além, Alice te mostra o caminho,
mas só nas QUARTAS-FEIRAS:

```

[loghost@home]$ g++ -S 1.cpp
[loghost@home]$ vim 1.s

```

Ele está gerando o assembly do dito código. Este é o programa antes de ser
"montado", ou antes do "assembler" operar.  Pra que isso serve? Se você
analisar as linhas cuidadosamente, vai enxergar os métodos que o
compilador, e as respectivas funões, usam para identificar os tipos de
variáveis.

Outra coisa interessante: os if's do C++ são preguiçosos. Isso significa
que executar

```C++

if (numer > 0 || number == 0)

```

é mais rápido que

```C++

if (number == 0 || number > 0)

```


