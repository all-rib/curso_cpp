# Testando seus programas

Para testar seu programa, primeiro compilamos seu programa:

```
g++ 1.cpp -o 1
```

E, para os sitemas unix, a correção é mais simplificada: rodamos, com a
devida entrada, jogando para uma saída temporária, e comparamos com o
gabarito. Se o 'cmp' não retornar nada, está correto. Caso contrário, ele
retornará a linha e a coluna onde encontrou a primeira diferença entre os
arquivos.

```
./1 < 1.test > 1.temp
cmp 1.temp 1.answ
1.answ e 1.temp são diferentes: byte 57, linha 15
```

Para o Windows ~~recomendo que você o abandone e vá para o GNU/Linux!~~ use
o PowerShell:

```
Get-Content 1.test | ./1 > 1.temp
fc 1.temp 1.answ
```
